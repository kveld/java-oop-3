package agh.edu.javaoop3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by kveld on 4/26/16.
 */
public class BFSIterator implements CompanyIterator {
    private Company company;
    private Queue<IEmployee> bfsQueue;

    public BFSIterator() {
        this.company = Company.getInstance();
        this.bfsQueue = new LinkedList<>();
    }

    @Override
    public Iterator<IEmployee> getIterator() {
        return new Iterator<IEmployee>() {
            @Override
            public boolean hasNext() {
                return !bfsQueue.isEmpty();
            }

            @Override
            public IEmployee next() {
                IEmployee toReturn = bfsQueue.remove();
                if(toReturn instanceof Manager) {
                    bfsQueue.addAll(((Manager) toReturn).getSubordinateList());
                }
                return toReturn;
            }
        };
    }

    @Override
    public void initialize() {
        if(this.company.getCeo().isPresent()) {
            bfsQueue.add(company.getCeo().get());
        }
    }
}
