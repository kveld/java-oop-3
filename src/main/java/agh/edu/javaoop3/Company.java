package agh.edu.javaoop3;

import com.google.common.base.Preconditions;

import java.util.*;

/**
 * Created by kveld on 4/22/16.
 */
public class Company implements Iterable<IEmployee> {
    private static Company instance = new Company();
    private Optional<Manager> ceo = Optional.empty();
    private CompanyIterator companyIterator;

    /*
     *  CONSTRUCTORS
     */

    private Company() {}

    /*
     *  GETTERS & SETTERS
     */

    public static Company getInstance() { return instance; }

    public Optional<Manager> getCeo() {
        return Company.getInstance().ceo;
    }

    public void setCeo(Manager m) {
        Company.getInstance().ceo = Optional.of(m);
    }

    public CompanyIterator getCompanyIterator() {
        return Company.getInstance().companyIterator;
    }

    public void setCompanyIterator(CompanyIterator iterator) {
        Company.getInstance().companyIterator = iterator;
    }

    /*
     *  REGULAR METHODS
     */

    public void accept(CompanyVisitor visitor) {
        visitor.visit(Company.getInstance());
    }

    public Iterator<IEmployee> iterator() {
        Preconditions.checkArgument(Company.getInstance().companyIterator != null);
        Company.getInstance().companyIterator.initialize();
        return Company.getInstance().companyIterator.getIterator();
    }
}
