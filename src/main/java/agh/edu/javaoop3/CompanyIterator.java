package agh.edu.javaoop3;

import java.util.Iterator;

/**
 * Created by kveld on 4/24/16.
 */
public interface CompanyIterator {
    /*
     *  Extracted iteration method from the Company class
     *  since it is very probable that we will want
     *  to use different orders of iteration
     */
    Iterator<IEmployee> getIterator();
    void initialize();
}
