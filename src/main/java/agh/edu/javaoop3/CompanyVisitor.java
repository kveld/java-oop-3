package agh.edu.javaoop3;

/**
 * Created by kveld on 4/23/16.
 */
public interface CompanyVisitor {
    void visit(IEmployee e);
    void visit(Manager e);
    void visit(Employee e);
    void visit(Company c);
}
