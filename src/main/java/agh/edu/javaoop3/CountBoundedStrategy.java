package agh.edu.javaoop3;

/**
 * Created by kveld on 4/22/16.
 */
public class CountBoundedStrategy implements HiringStrategy {
    private int maxEmployeeCnt;

    public CountBoundedStrategy(int maxEmployeeCnt) {
        this.maxEmployeeCnt = maxEmployeeCnt;
    }

    public boolean canHire(Manager m, IEmployee e) {
        return m.getSubordinateList().size() < maxEmployeeCnt;
    }
}
