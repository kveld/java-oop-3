package agh.edu.javaoop3;

import java.util.*;

/**
 * Created by kveld on 4/24/16.
 */
public class DFSIterator implements CompanyIterator {
    /*
     *  Implemented because the first draft of homework mentioned DFS iteration
     *  Left it in because why not
     */
    private Company company;
    private Stack<IEmployee> dfsStack;

    public DFSIterator() {
        this.dfsStack = new Stack<>();
        this.company = Company.getInstance();
    }

    @Override
    public void initialize() {
        if(company.getCeo().isPresent()) {
            dfsStack.push(company.getCeo().get());
        }
    }

    @Override
    public Iterator<IEmployee> getIterator() {
        return new Iterator<IEmployee>() {
            @Override
            public boolean hasNext() {
                return !dfsStack.empty();
            }

            @Override
            public IEmployee next() {
                IEmployee toReturn = dfsStack.pop();
                if(toReturn instanceof Manager) {
                    /*
                     * The code below creates a reference to the Manager's subordinates list
                     * and pushes them to the stack in reverse order, so the iteration
                     * conforms to the order they were hired in
                     *
                     * Explicit iteration done so as not to mess with copying and Collections.reverse
                     * A reference created so I don't have to write the getSubordinateList with cast too many times
                     */
                    List<IEmployee> localSubordinates = ((Manager) toReturn).getSubordinateList();
                    for(int i = localSubordinates.size() - 1; i >= 0; i--) {
                        dfsStack.push(localSubordinates.get(i));
                    }
                }
                return toReturn;
            }
        };
    }
}
