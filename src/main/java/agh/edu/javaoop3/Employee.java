package agh.edu.javaoop3;

import java.math.BigDecimal;

/**
 * Created by kveld on 4/22/16.
 */
public class Employee implements IEmployee{
    private final String name;
    private BigDecimal salary;
    private SatisfactionStrategy satisfactionStrategy;

    public Employee(String name, BigDecimal salary, SatisfactionStrategy satisfactionStrategy) {
        this.name = name;
        this.salary = salary;
        this.satisfactionStrategy = satisfactionStrategy;
    }


    @Override
    public boolean isSatisfied() {
        return satisfactionStrategy.isSatisfied(this);
    }

    @Override
    public void setSatisfactionStrategy(SatisfactionStrategy satisfactionStrategy) {
        this.satisfactionStrategy = satisfactionStrategy;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getSalary() {
        return salary;
    }

    @Override
    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public void accept(CompanyVisitor visitor) {
        visitor.visit(this);
    }
}
