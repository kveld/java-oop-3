package agh.edu.javaoop3;

/**
 * Created by kveld on 4/22/16.
 */
public interface HiringStrategy {
    boolean canHire(Manager m, IEmployee e);
}
