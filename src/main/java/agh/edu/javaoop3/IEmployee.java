package agh.edu.javaoop3;

import java.math.BigDecimal;

/**
 * Created by kveld on 4/22/16.
 */
public interface IEmployee {
    boolean isSatisfied();
    void setSatisfactionStrategy(SatisfactionStrategy satisfactionStrategy);
    String getName();
    BigDecimal getSalary();
    void setSalary(BigDecimal salary);
    void accept(CompanyVisitor visitor);
}
