package agh.edu.javaoop3;

import com.google.common.base.Preconditions;

/**
 * Created by kveld on 4/27/16.
 */
public class LowestSalaryVisitor implements CompanyVisitor {
    private IEmployee lowestSalaryEmployee;

    public IEmployee getLowestSalaryEmployee() {
        Preconditions.checkState(lowestSalaryEmployee != null);
        return lowestSalaryEmployee;
    }

    @Override
    public void visit(IEmployee e) {
        /*
         *  Implemented so visiting manager's subordinates
         *  can be done without instanceof
         */
        e.accept(this);
    }

    @Override
    public void visit(Manager e) {
        if(e.getSalary().compareTo(lowestSalaryEmployee.getSalary()) == -1) {
            lowestSalaryEmployee = e;
        }
        for(IEmployee subordinate : e.getSubordinateList()) {
            this.visit(subordinate);
        }
    }

    @Override
    public void visit(Employee e) {
        if(e.getSalary().compareTo(lowestSalaryEmployee.getSalary()) == -1) {
            lowestSalaryEmployee = e;
        }
    }

    @Override
    public void visit(Company c) {
        Preconditions.checkArgument(c.getCeo().isPresent());
        this.lowestSalaryEmployee = c.getCeo().get();
        this.visit(c.getCeo().get());
    }
}
