package agh.edu.javaoop3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Created by kveld on 4/22/16.
 */
public class Manager implements IEmployee{
    private IEmployee employee;
    private HiringStrategy hiringStrategy;
    private List<IEmployee> subordinateList;



    /*
         *  CONSTRUCTORS
         */
    public Manager(String name, BigDecimal salary, SatisfactionStrategy satisfactionStrategy, HiringStrategy hiringStrategy) {
        this.employee = new Employee(name, salary, satisfactionStrategy);
        this.hiringStrategy = hiringStrategy;
        this.subordinateList = new ArrayList<>();
    }

    /*
     *  GETTERS & SETTERS
     */

    public List<IEmployee> getSubordinateList() {
        return ImmutableList.copyOf(subordinateList);
    }

    public void setHiringStrategy(HiringStrategy hiringStrategy) {
        this.hiringStrategy = hiringStrategy;
    }
    public HiringStrategy getHiringStrategy() { return this.hiringStrategy; }

    /*
     *  REGULAR METHODS
     */

    @Override
    public boolean isSatisfied() {
        return employee.isSatisfied();
    }

    @Override
    public void setSatisfactionStrategy(SatisfactionStrategy satisfactionStrategy) {
        employee.setSatisfactionStrategy(satisfactionStrategy);
    }

    @Override
    public String getName() {
        return employee.getName();
    }

    @Override
    public BigDecimal getSalary() {
        return employee.getSalary();
    }

    @Override
    public void setSalary(BigDecimal salary) {
        employee.setSalary(salary);
    }

    public void accept(CompanyVisitor visitor) {
        visitor.visit(this);
    }

    public boolean canHire(IEmployee e) {
        return hiringStrategy.canHire(this, e);
    }

    public void hire(IEmployee e) {
        Preconditions.checkArgument(canHire(e));
        subordinateList.add(e);
    }

}
