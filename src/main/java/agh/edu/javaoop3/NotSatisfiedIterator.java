package agh.edu.javaoop3;


import java.util.Iterator;

/**
 * Created by kveld on 4/27/16.
 */
public class NotSatisfiedIterator implements CompanyIterator {
    private PredicateIterator iterator;

    public NotSatisfiedIterator() {
        this.iterator = new PredicateIterator((employee) -> !employee.isSatisfied());
    }

    @Override
    public Iterator<IEmployee> getIterator() {
        return iterator.getIterator();
    }

    @Override
    public void initialize() {
        iterator.initialize();
    }
}
