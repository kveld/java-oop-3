package agh.edu.javaoop3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by kveld on 4/27/16.
 */
public class PredicateIterator implements CompanyIterator {
    private Company c;
    private List<IEmployee> predicateSatisfyingList;
    private Predicate<IEmployee> predicate;

    public PredicateIterator(Predicate<IEmployee> predicate) {
        this.predicate = predicate;
        this.predicateSatisfyingList = new ArrayList<>();
        this.c = Company.getInstance();
    }

    private void constructList(IEmployee e) {
        if(predicate.test(e)) {
            predicateSatisfyingList.add(e);
        }
        if(e instanceof Manager) {
            for(IEmployee subordinate : ((Manager) e).getSubordinateList()) {
                constructList(subordinate);
            }
        }
    }

    @Override
    public Iterator<IEmployee> getIterator() {
        return predicateSatisfyingList.iterator();
    }

    @Override
    public void initialize() {
        if(!predicateSatisfyingList.isEmpty()) {
            /*
             *  The list has to be cleared and constructed again
             *  in order for the iterator to work several times in a row
             *  in case the company structure changes
             */
            predicateSatisfyingList.clear();
        }
        if(c.getCeo().isPresent()) {
            constructList(c.getCeo().get());
        }
    }
}
