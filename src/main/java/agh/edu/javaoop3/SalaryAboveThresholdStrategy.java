package agh.edu.javaoop3;

import java.math.BigDecimal;

/**
 * Created by kveld on 4/22/16.
 */
public class SalaryAboveThresholdStrategy implements SatisfactionStrategy {
    private BigDecimal threshold;

    public SalaryAboveThresholdStrategy(BigDecimal threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean isSatisfied(IEmployee IEmployee) {
        return IEmployee.getSalary().compareTo(threshold) != -1;
    }
}
