package agh.edu.javaoop3;

import java.math.BigDecimal;

/**
 * Created by kveld on 4/22/16.
 */
public class SalaryBoundedStrategy implements HiringStrategy {
    private BigDecimal maxSalarySum;

    public SalaryBoundedStrategy(BigDecimal maxSalarySum) {
        this.maxSalarySum = maxSalarySum;
    }

    @Override
    public boolean canHire(Manager m, IEmployee e) {
        BigDecimal newSum = m.getSubordinateList()
                             .stream()
                             .map(employee -> employee.getSalary())
                             .reduce(BigDecimal.ZERO, BigDecimal::add)
                             .add(e.getSalary());
        return newSum.compareTo(maxSalarySum) != 1;
    }
}
