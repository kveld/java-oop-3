package agh.edu.javaoop3;

/**
 * Created by kveld on 4/22/16.
 */
public interface SatisfactionStrategy {
    boolean isSatisfied(IEmployee IEmployee);
}
