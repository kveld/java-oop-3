package agh.edu.javaoop3;

import com.google.common.base.Preconditions;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Created by kveld on 4/27/16.
 */
public class TotalSalaryVisitor implements CompanyVisitor {
    private Optional<BigDecimal> totalSalary;

    public TotalSalaryVisitor() {
        this.totalSalary = Optional.empty();
    }

    public BigDecimal getTotalSalary() {
        Preconditions.checkState(totalSalary.isPresent());
        return totalSalary.get();
    }

    @Override
    public void visit(IEmployee e) {
        /*
         *  Implemented so visiting manager's subordinates
         *  can be done without instanceof
         */
        e.accept(this);
    }

    @Override
    public void visit(Manager e) {
        totalSalary = Optional.of(totalSalary.get().add(e.getSalary()));
        for(IEmployee subordinate : e.getSubordinateList()) {
            this.visit(subordinate);
        }
    }

    @Override
    public void visit(Employee e) {
        totalSalary = Optional.of(totalSalary.get().add(e.getSalary()));
    }

    @Override
    public void visit(Company c) {
        Preconditions.checkArgument(c.getCeo().isPresent());
        this.totalSalary = Optional.of(BigDecimal.ZERO);
        this.visit(c.getCeo().get());
    }
}
