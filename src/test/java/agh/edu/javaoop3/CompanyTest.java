package agh.edu.javaoop3;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Iterator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by kveld on 4/23/16.
 */
public class CompanyTest {
    private Company ellpa;
    private SatisfactionStrategy mockedSatisfcationStrategy;
    private HiringStrategy liberalHiringStrategy;
    private BigDecimal mockSalary;
    private Manager ceo;
    private Manager underCeo1;
    private IEmployee underCeo2;
    private IEmployee underManager1;
    private IEmployee underManager2;

    @Before
    public void setUp() throws Exception {
        /*
         *  Set up needed dependencies
         */

        mockSalary = mock(BigDecimal.class);
        mockedSatisfcationStrategy = mock(SatisfactionStrategy.class);
        liberalHiringStrategy = new CountBoundedStrategy(Integer.MAX_VALUE);

        /*
         *  Create company skeleton
         */

        ceo = new Manager("Stefan", mockSalary, mockedSatisfcationStrategy, liberalHiringStrategy);
        ellpa = Company.getInstance();
        ellpa.setCeo(ceo);

        /*
         *  Create company structure
         */

        underCeo1 = new Manager("jan", mockSalary, mockedSatisfcationStrategy, liberalHiringStrategy);
        underCeo2 = new Employee("jan", mockSalary, mockedSatisfcationStrategy);
        underManager1 = new Employee("jan", mockSalary, mockedSatisfcationStrategy);
        underManager2 = new Employee("jan", mockSalary, mockedSatisfcationStrategy);

        underCeo1.hire(underManager1);
        underCeo1.hire(underManager2);

        ellpa.getCeo().get().hire(underCeo1);
        ellpa.getCeo().get().hire(underCeo2);
    }

    @Test
    public void testCompanyIterator() {
        // Given
        ellpa.setCompanyIterator(new BFSIterator());

        // When
        Iterator<IEmployee> bfsIterator = ellpa.iterator();

        // Then
        assertEquals(bfsIterator.next(), ceo);
        assertEquals(bfsIterator.next(), underCeo1);
        assertEquals(bfsIterator.next(), underCeo2);
        assertEquals(bfsIterator.next(), underManager1);
        assertEquals(bfsIterator.next(), underManager2);
        assertFalse(bfsIterator.hasNext());
    }

    @Test
    public void testPredicateIterator() {
        //Given
        BigDecimal testThreshold = new BigDecimal(5000.0);
        BigDecimal belowThreshold = new BigDecimal(4999.999);
        BigDecimal aboveThreshold = new BigDecimal(5000.0009);
        ceo.setSalary(aboveThreshold);
        underCeo1.setSalary(belowThreshold);
        underCeo2.setSalary(aboveThreshold);
        underManager1.setSalary(belowThreshold);
        underManager2.setSalary(testThreshold);
        PredicateIterator iterator = new PredicateIterator((employee) -> employee.getSalary().compareTo(testThreshold) == 1);

        // When
        ellpa.setCompanyIterator(iterator);

        //Then
        for(IEmployee e : ellpa) {
            assertTrue(e.getSalary().compareTo(testThreshold) == 1);
        }
    }

    @Test
    public void testNotSatisfiedIterator() {
        SatisfactionStrategy ss = new SalaryAboveThresholdStrategy(new BigDecimal(5000.0));
        BigDecimal satisfyingSalary = new BigDecimal(5000.09);
        BigDecimal unsatisfyingSalary = new BigDecimal(4999.999);
        ceo.setSatisfactionStrategy(ss);
        ceo.setSalary(satisfyingSalary);
        underCeo1.setSatisfactionStrategy(ss);
        underCeo1.setSalary(satisfyingSalary);
        underCeo2.setSatisfactionStrategy(ss);
        underCeo2.setSalary(unsatisfyingSalary);
        underManager1.setSatisfactionStrategy(ss);
        underManager1.setSalary(unsatisfyingSalary);
        underManager2.setSatisfactionStrategy(ss);
        underManager2.setSalary(unsatisfyingSalary);
        ellpa.setCompanyIterator(new NotSatisfiedIterator());

        for(IEmployee e : ellpa) {
            assertFalse(e.isSatisfied());
        }
    }


    @Test
    public void testTotalSalaryVisitor() {
        // Given
        BigDecimal testSalary = new BigDecimal(10000.0);
        BigDecimal totalSalary = new BigDecimal(50000.0);
        ceo.setSalary(testSalary);
        underCeo1.setSalary(testSalary);
        underCeo2.setSalary(testSalary);
        underManager1.setSalary(testSalary);
        underManager2.setSalary(testSalary);
        TotalSalaryVisitor visitor = new TotalSalaryVisitor();

        // When
        ellpa.accept(visitor);

        // Then
        assertTrue(visitor.getTotalSalary().compareTo(totalSalary) == 0);
    }

    @Test
    public void testLowestSalaryVisitor() {
        //Given
        BigDecimal testSalary = new BigDecimal(10000.0);
        BigDecimal minSalary = new BigDecimal(9999.9999);
        ceo.setSalary(testSalary);
        underCeo1.setSalary(testSalary);
        underCeo2.setSalary(testSalary);
        underManager1.setSalary(testSalary);
        underManager2.setSalary(minSalary);
        LowestSalaryVisitor visitor = new LowestSalaryVisitor();

        // When
        ellpa.accept(visitor);

        // Then
        assertEquals(visitor.getLowestSalaryEmployee(), underManager2);
    }

}