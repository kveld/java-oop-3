package agh.edu.javaoop3;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by kveld on 4/23/16.
 */
public class EmployeeTest {
    /*
     *  This is actually more of a satisfaction strategies test,
     *  but their sole reason for existence is providing behaviour
     *  for employee
     */
    private SatisfactionStrategy satisfactionStrategy;
    private Employee dissatisfiedEmployee;
    private Employee satisfiedEmployee;

    @Before
    public void setUp() throws Exception {
        satisfactionStrategy = new SalaryAboveThresholdStrategy(new BigDecimal(10000.0));
        dissatisfiedEmployee = new Employee("Zbyszko", new BigDecimal(9999.99), satisfactionStrategy);
        satisfiedEmployee = new Employee("Mateusz", new BigDecimal(10000.01), satisfactionStrategy);
    }

    @Test
    public void testSatisfaction() {
        assertFalse(dissatisfiedEmployee.isSatisfied());
        assertTrue(satisfiedEmployee.isSatisfied());
    }
}