package agh.edu.javaoop3;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by kveld on 4/23/16.
 */
public class ManagerTest {
    /*
     * This is actually more of a hiring strategies test,
     * but their sole reason for existence is providing
     * behaviour for the manager
     */
    private Manager countBounded;
    private Manager salaryBounded;
    private List<IEmployee> hirees;
    private SatisfactionStrategy strategy = mock(SatisfactionStrategy.class);

    @Before
    public void setUp() throws Exception {
        hirees = new ArrayList<>();
        countBounded = new Manager("Janusz", new BigDecimal(10000.0), strategy, new CountBoundedStrategy(4));
        salaryBounded = new Manager("Krystian", new BigDecimal(20000.0), strategy, new SalaryBoundedStrategy(new BigDecimal(100000.0)));

        SatisfactionStrategy s = new SalaryAboveThresholdStrategy(new BigDecimal(10000.0));
        hirees.add(new Employee("Michal", new BigDecimal(14000.0), s));
        hirees.add(new Employee("Mordechaj", new BigDecimal(36000.0), s));
        hirees.add(new Employee("Konrad", new BigDecimal(40000.0), s));
        hirees.add(new Employee("Bartlomiej", new BigDecimal(10000.1), s));
        hirees.add(new Employee("Mateo", new BigDecimal(9999.99), s));

        countBounded.hire(hirees.get(0));
        countBounded.hire(hirees.get(1));
        countBounded.hire(hirees.get(2));

        salaryBounded.hire(hirees.get(0));
        salaryBounded.hire(hirees.get(1));
        salaryBounded.hire(hirees.get(2));
    }

    @Test
    public void testSalaryBound() {
        assertFalse(salaryBounded.canHire(hirees.get(3)));
        assertTrue(salaryBounded.canHire(hirees.get(4)));
    }

    @Test
    public void testCountBound() {
        assertTrue(countBounded.canHire(hirees.get(3)));
        countBounded.hire(hirees.get(3));
        assertFalse(countBounded.canHire(hirees.get(4)));
    }
}